import { Doughnut, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  extends: Doughnut,
  props: {
    chartdata: {
      type: Object,
      default: null
    },
    options: {
      type: Object,
      default: null
    }
  },
  data(){
    return {
      htmlLegend: null
    }
  },
  mixins: [reactiveProp],
  mounted () {
    this.renderChart(this.chartData, this.options)
    this.htmlLegend = this.generateLegend()
  },
  watch: {
    chartData () {
      this.$data._chart.update()
    }
  }
}
