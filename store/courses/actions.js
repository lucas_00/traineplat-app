export default {
  async getCourses({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/course/index', data)
        commit('SET_COURSES', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async createCourse({ commit, dispatch }, formData) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/course/store', formData)
        commit('ADD_COURSE', response.data.course)
        await dispatch('layout/getLayoutConfig', null, { root: true })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async deleteCourse({ dispatch }, course_id) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/course/destroy', {
          params: {
            course_id
          }
        })
        await dispatch('layout/getLayoutConfig', null, { root: true })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
