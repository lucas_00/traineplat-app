import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    courses: []
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
