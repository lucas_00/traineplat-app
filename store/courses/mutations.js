export default {
  SET_COURSES(state, data) {
    state.courses = data.courses;
  },
  ADD_COURSE(state, data) {
    state.courses.push(data);
  }
}
