import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    groups: [],
    courses: []
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
