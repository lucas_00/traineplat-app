export default {
  SET_LAYOUT_CONFIG(state, data) {
    state.groups = data.groups;
    state.courses = data.courses;
  }
}
