export default {
  async getLayoutConfig({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/layout/index', data)
        commit('SET_LAYOUT_CONFIG', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
