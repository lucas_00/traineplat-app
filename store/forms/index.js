import actions from '@/store/forms/actions'
import mutations from '@/store/forms/mutations'

const state = function(){
  return {
    forms: [],
    form: {},
    groups: [],
    group: null,
    allFormSubmits: [],
    formSubmit: {}
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
