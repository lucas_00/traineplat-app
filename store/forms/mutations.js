export default {
  SET_FORMS(state, data) {
    state.forms = data;
  },

  SET_FORM(state, data) {
    state.form = data;
  },

  SET_GROUPS(state, data) {
    state.groups = data;
  },

  SET_ALL_FORM_SUBMITS(state, data) {
    state.allFormSubmits = data;
  },

  SET_FORM_SUBMIT(state, data) {
    state.formSubmit = data;
  },

  UPDATE_FORM(state, data) {
    state.form.name = data.name;
    state.form.description = data.description;
  },

  ADD_FORM(state, data) {
    state.forms.push(data);
  },

  REMOVE_FORM(state) {
    state.form = {};
  },

  // Form Content
  ADD_FORM_CONTENT(state, data) {
    state.form.contents.push(data);
  },

  REMOVE_FORM_CONTENT(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.id)
    state.form.contents.splice(index, 1)
  },

  INIT_FORM_ANSWERS(state){
    for (let i = 0; i < state.form.contents.length; i++) {
      let form_content = state.form.contents[i];
      if (form_content.form_contentable_type.includes('FormContentFillInTheBlankOption')) {
        for (let i = 0; i < form_content.form_contentable.question.options.length; i++) {
          form_content.form_contentable.question.options[i]['answers'] = '';
        }
      }else if (form_content.form_contentable_type.includes('FormContentFillInTheBlank')) {
        for (let i = 0; i < form_content.form_contentable.question.options.length; i++) {
          form_content.form_contentable.question.options[i]['answers'] = '';
        }
      }else if (form_content.form_contentable_type.includes('FormContentSelect')) {
        form_content.form_contentable.question['answers'] = '';
      }else if(form_content.form_contentable_type.includes('FormContentMultiple')){
        form_content.form_contentable.question['answers'] = [];
      }else if(form_content.form_contentable_type.includes('FormContentQuestion')){
        form_content.form_contentable.question['answers'] = '';
      }
    }
  },

  UPDATE_FORM_CONTENT(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.id)
    state.form.contents[index].form_contentable.content = data.form_contentable.content;
  },

  APPEND_FORM_CONTENT_IMAGE(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.content.id)
    for (let i = 0; i < data.images.length; i++) {
      const image = data.images[i];
      state.form.contents[index].form_contentable.contents.push(image);
    }
  },

  DESTROY_FORM_CONTENT_IMAGE(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content.id)
    let indexImage = state.form.contents[index].form_contentable.contents.findIndex(x => x.id == data.image.id)
    state.form.contents[index].form_contentable.contents.splice(indexImage, 1);
  },

  // Questions
  UPDATE_FORM_CONTENT_QUESTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    state.form.contents[index].form_contentable.question.content = data.question.content;
  },

  // Options
  ADD_FORM_CONTENT_QUESTION_OPTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    state.form.contents[index].form_contentable.question.options.push(data.option)
  },

  UPDATE_FORM_CONTENT_QUESTION_OPTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let option_index = state.form.contents[index].form_contentable.question.options.findIndex(x => x.id == data.option.id)
    state.form.contents[index].form_contentable.question.options[option_index].content = data.option.content;
  },

  UPDATE_FORM_CONTENT_QUESTION_OPTION_CORRECT_SELECT(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let options = state.form.contents[index].form_contentable.question.options;

    let option_index = options.findIndex(x => x.id == data.option.id)

    for (let i = 0; i < options.length; i++) {
      let option = options[i];
      if (option_index == i) {
        option.correct.is_correct = '1';
      }else {
        option.correct.is_correct = '0';
      }
    }
  },

  UPDATE_FORM_CONTENT_QUESTION_OPTION_CORRECT_MULTIPLE(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let option_index = state.form.contents[index].form_contentable.question.options.findIndex(x => x.id == data.option.id)
    state.form.contents[index].form_contentable.question.options[option_index].correct.is_correct = data.option.correct.is_correct;
  },

  REMOVE_FORM_CONTENT_QUESTION_OPTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let option_index = state.form.contents[index].form_contentable.question.options.findIndex(x => x.id == data.option.id)
    state.form.contents[index].form_contentable.question.options.splice(option_index, 1)
  },

  UPDATE_ANSWER_CORRECT(state, data) {
    let index = state.formSubmit.answers.findIndex(x => x.id == data.answer.id)
    state.formSubmit.answers[index].correct.is_correct = data.answer.correct.is_correct;
  },

  UPDATE_FORM_SUBMIT_CHECKED(state, data) {
    state.formSubmit.is_checked = data.formSubmit.is_checked;
  },

  SET_SELECTED_GROUP(state, data) {
    state.group = data;
  },
}
