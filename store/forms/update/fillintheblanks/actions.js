export default {
  // Question
  async updateFillintheblankQuestion({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/fillintheblank_question_content_update', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async deleteFillintheblankContent({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/form_content/destroy_fillintheblank_content', {
          params: data
        })
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  //FormContent
  async updateContentFillintheblankTextContent({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/content_fillintheblank_text_content_update', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentFillintheblankSelect({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/form_content/add_form_content_fillintheblank_select', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentFillintheblankInput({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/form_content/add_form_content_fillintheblank_input', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  // Option
  async addFillintheblankOption({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/form_content/add_fillintheblank_option', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async updateFillintheblankOption({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/update_fillintheblank_option', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async updateCorrectFillintheblankSelect({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/update_correct_fillintheblank_select', data)
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async deleteFillintheblankOption({ commit, rootState }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/form_content/destroy_fillintheblank_option', {
          params: data
        })
        commit('UPDATE_FORM_CONTENT', {
          "rootState": rootState,
          "data": response.data
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
