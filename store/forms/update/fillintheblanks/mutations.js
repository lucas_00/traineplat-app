export default {
  // Questions
  UPDATE_FORM_CONTENT_QUESTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    state.form.contents[index].form_contentable.question.content = data.question.content;
  },

  // FormContent
  UPDATE_FORM_CONTENT(state, payload) {
    const index = payload.rootState.forms.form.contents.findIndex(x => x.id == payload.data.content.id)
    payload.rootState.forms.form.contents[index].form_contentable.contents = payload.data.content.form_contentable.contents;
  },

  // Options
  ADD_FORM_CONTENT_QUESTION_OPTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    state.form.contents[index].form_contentable.question.options.push(data.option)
  },

  UPDATE_FORM_CONTENT_QUESTION_OPTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let option_index = state.form.contents[index].form_contentable.question.options.findIndex(x => x.id == data.option.id)
    state.form.contents[index].form_contentable.question.options[option_index].content = data.option.content;
  },

  UPDATE_FORM_CONTENT_QUESTION_OPTION_CORRECT(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let option_index = state.form.contents[index].form_contentable.question.options.findIndex(x => x.id == data.option.id)
    state.form.contents[index].form_contentable.question.options[option_index].correct.is_correct = data.option.correct.is_correct;
  },

  REMOVE_FORM_CONTENT_QUESTION_OPTION(state, data) {
    let index = state.form.contents.findIndex(x => x.id == data.form_content_id)
    let option_index = state.form.contents[index].form_contentable.question.options.findIndex(x => x.id == data.option.id)
    state.form.contents[index].form_contentable.question.options.splice(option_index, 1)
  }
}
