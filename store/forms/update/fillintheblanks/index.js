import actions from '@/store/forms/actions'
import mutations from '@/store/forms/mutations'

const state = function(){
  return {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
