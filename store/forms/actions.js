export default {
  async getForms({ commit }) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/form/index')
        commit('SET_FORMS', response.data.forms)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async getForm({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/form/show', data)
        commit('SET_FORM', response.data.form)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async getFormStatistics({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/form/statistics', data)
        commit('SET_FORM', response.data.form)
        commit('SET_GROUPS', response.data.groups)
        commit('SET_ALL_FORM_SUBMITS', response.data.allFormSubmits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async getFormSubmits({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/form/submit_answers', data)
        commit('SET_FORM', response.data.form)
        commit('SET_GROUPS', response.data.groups)
        commit('SET_ALL_FORM_SUBMITS', response.data.allFormSubmits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async getFormSubmit({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/form/show_submit', data)
        commit('SET_FORM', response.data.form)
        commit('SET_FORM_SUBMIT', response.data.formSubmit)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async getFormSubmitGroup({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/group/show_submit', data)
        commit('SET_FORM', response.data.form)
        commit('SET_FORM_SUBMIT', response.data.formSubmit)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async submitGroupForm({ state }, payload) {
    return new Promise(async(resolve, reject)=>{
      try {
        let data = [];

        for (let i = 0; i < state.form.contents.length; i++) {
          const form_content = state.form.contents[i];
          data.push(form_content.form_contentable.question);
        }

        let formData = new FormData();
        formData.append('questions', JSON.stringify(data));
        formData.append('group_id', payload.group_id);
        formData.append('form_id', payload.form_id);
        const response = await this.$axios.post('/api/form/submit_form', formData)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async getGroupForm({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/group/form', data)
        commit('SET_FORM', response.data.form)
        commit('INIT_FORM_ANSWERS');

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addForm({ commit }, form) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/form/store', form)
        // commit('ADD_FORM', response.data.form)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async destroyForm({ commit, state }) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/form/destroy', {
          params: {
            'form_id': state.form.id,
          }
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async updateForm({ commit }, form) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/form/update', form)
        commit('UPDATE_FORM', response.data.form)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  // Form Content
  async addFormContentFillintheblank({ commit, state }, form_content_fillintheblanks) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('form_content_fillintheblanks', JSON.stringify(form_content_fillintheblanks));
        const response = await this.$axios.post('/api/form_content/content_fillintheblank_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentFillintheblankOption({ commit, state }, content) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('name', content.name);
        formData.append('options', JSON.stringify(content.options));
        const response = await this.$axios.post('/api/form_content/content_fillintheblank_option_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentQuestion({ commit, state }, content) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('name', content.name);
        const response = await this.$axios.post('/api/form_content/content_question_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentSelect({ commit, state }, content) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('name', content.name);
        formData.append('options', JSON.stringify(content.options));
        const response = await this.$axios.post('/api/form_content/content_select_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentMultiple({ commit, state }, content) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('name', content.name);
        formData.append('options', JSON.stringify(content.options));
        const response = await this.$axios.post('/api/form_content/content_multiple_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentText({ commit, state }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('content', data.content);
        const response = await this.$axios.post('/api/form_content/content_text_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async editFormContentText({ commit, state }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/content_text_update', {
          'form_id': state.form.id,
          'form_content_id': data.form_content_id,
          'content': data.content
        })
        commit('UPDATE_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentLink({ commit, state }, content) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('content', content.content);
        const response = await this.$axios.post('/api/form_content/content_link_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async editFormContentLink({ commit, state }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/content_link_update', {
          'form_id': state.form.id,
          'form_content_id': data.form_content_id,
          'content': data.content
        })
        commit('UPDATE_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentVideo({ commit, state }, content) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        formData.append('content', content.content);
        const response = await this.$axios.post('/api/form_content/content_video_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async editFormContentVideo({ commit, state }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/content_video_update', {
          'form_id': state.form.id,
          'form_content_id': data.form_content_id,
          'content': data.content
        })
        commit('UPDATE_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async addFormContentImage({ commit, state }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_id', state.form.id);
        for (let i = 0; i < data.contents.length; i++) {
          formData.append('files-' + i, data.contents[i]);
        }
        const response = await this.$axios.post('/api/form_content/content_image_store', formData)
        commit('ADD_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async appendFormContentImage({ commit, state }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_content_id', data.form_content_id);
        for (let i = 0; i < data.contents.length; i++) {
          formData.append('files-' + i, data.contents[i]);
        }
        formData.append('_method', 'PUT');
        const response = await this.$axios.post('/api/form_content/content_image_append', formData)
        commit('APPEND_FORM_CONTENT_IMAGE', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async destroyFormContentImage({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/form_content/content_image_destroy', {
          params: {
            'image_id': data.image.id,
          }
        })
        commit('DESTROY_FORM_CONTENT_IMAGE', {
          'form_content': data.form_content,
          'image': response.data.image
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async destroyFormContent({ commit }, form_content_id) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/form_content/destroy', {
          params: {
            'form_content_id': form_content_id,
          }
        })
        commit('REMOVE_FORM_CONTENT', response.data.content)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  // Question
  async updateQuestion({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/update_question', data)
        commit('UPDATE_FORM_CONTENT_QUESTION', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  // Option
  async addOption({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/form_content/add_option', data)
        commit('ADD_FORM_CONTENT_QUESTION_OPTION', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async updateOption({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/update_option', data)
        commit('UPDATE_FORM_CONTENT_QUESTION_OPTION', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async updateCorrectSelect({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/update_correct_select', data)
        commit('UPDATE_FORM_CONTENT_QUESTION_OPTION_CORRECT_SELECT', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async updateCorrectMultiple({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/update_correct_multiple', data)
        commit('UPDATE_FORM_CONTENT_QUESTION_OPTION_CORRECT_MULTIPLE', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async setCorrectAnswer({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/set_correct_answer', data)
        commit('UPDATE_ANSWER_CORRECT', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async setChecked({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.put('/api/form_content/set_checked', data)
        commit('UPDATE_FORM_SUBMIT_CHECKED', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async deleteOption({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/form_content/destroy_option', {
          params: data
        })
        commit('REMOVE_FORM_CONTENT_QUESTION_OPTION', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  // Answers
  setSelectedGroup({ commit }, data) {
    commit('SET_SELECTED_GROUP', data)
  }
}
