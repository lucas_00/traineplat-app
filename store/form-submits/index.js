import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    userFormSubmits: [],
    groupformSubmits: [],
    courseformSubmits: []
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
