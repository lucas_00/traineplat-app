export default {
  SET_USER_FORM_SUBMITS(state, data) {
    state.userFormSubmits = data;
  },
  SET_GROUP_FORM_SUBMITS(state, data) {
    state.groupFormSubmits = data;
  },
  SET_COURSE_FORM_SUBMITS(state, data) {
    state.courseFormSubmits = data;
  }
}
