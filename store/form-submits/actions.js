export default {
  async getUserFormSubmits({ commit }, data) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/submit/get_user_form_submits', {
          params: data
        })
        commit('SET_USER_FORM_SUBMITS', response.data.userFormSubmits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async getGroupFormSubmits({ commit }, data) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/submit/get_group_form_submits', {
          params: data
        })
        commit('SET_GROUP_FORM_SUBMITS', response.data.groupFormSubmits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async getCourseFormSubmits({ commit }, data) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/submit/get_course_form_submits', data)
        commit('SET_COURSE_FORM_SUBMITS', response.data.courseFormSubmits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
