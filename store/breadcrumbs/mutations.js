export default {
  SET_BREADCRUMBS(state, data) {
    state.breadcrumbs = data;
  },
  SET_ROLE(state, data) {
    state.role = data;
  }
}
