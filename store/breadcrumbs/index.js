import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    breadcrumbs: [],
    role: null
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
