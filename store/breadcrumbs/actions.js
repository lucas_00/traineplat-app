export default {
  async getBreadcrumbGroup({ commit }, data) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/breadcrumb/get_group', {
          params: data
        })
        commit('SET_BREADCRUMBS', response.data.breadcrumb)
        commit('SET_ROLE', response.data.role.role_user)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async getBreadcrumbGroupForm({ commit }, data) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/breadcrumb/get_group_course_unit_form', {
          params: data
        })
        commit('SET_BREADCRUMBS', response.data.breadcrumb)
        commit('SET_ROLE', response.data.role.role_user)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
