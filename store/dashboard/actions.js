export default {
  async getGroups({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/group/dashboard', data)
        commit('SET_GROUPS', response.data)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
