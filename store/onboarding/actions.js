export default {
  async forgotPassword({ commit }, email) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/forgot-password', {
          email
        })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async resetPassword({ commit }, payload) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/reset-password', payload)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
