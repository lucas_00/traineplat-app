import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
