import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = function(){
  return {
    users: [],
    group: [],
    userFormSubmits: []
  }
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
