export default {
  SET_USERS(state, data) {
    state.users = data;
  },
  SET_GROUP(state, data) {
    state.group = data;
  },
  SET_USER_FORM_SUBMITS(state, data) {
    state.userFormSubmits = data;
  }
}
