export default {
  async getUserProgress({ commit }, group_id) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/user_progress/index', {
          params: {
            'group_id': group_id,
          }
        })
        commit('SET_USERS', response.data.users)
        commit('SET_GROUP', response.data.group)
        commit('SET_USER_FORM_SUBMITS', response.data.userFormSubmits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async getUserFormSubmits({ commit }, payload) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await this.$axios.get('/api/user_progress/get_user_form_submits', {
          params: {
            'group_id': payload.group_id,
            'user_id': payload.user_id,
            'form_id': payload.form_id,
          }
        })
        commit('SET_USER_FORM_SUBMITS', response.data.user_form_submits)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
