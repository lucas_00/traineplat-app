export default {
  formatedUserProgress: (state, getters) => {
    let auxReturn = [];
    for (let i = 0; i < state.group.users.length; i++) {
      const user = state.group.users[i];
      // add User
      let auxUserProgress = {
        'name': `${user.email} - ${user.name}`,
        'icon': 'user',
        'children': []
      };
      // check Group
      for (let y = 0; y < state.group.courses.length; y++) {
        // add Courses
        const course = state.group.courses[y];
        auxUserProgress.children.push({
          'name': course.name,
          'icon': 'course',
          'children': []
        });
        for (let j = 0; j < course.contents.length; j++) {
          // add Units
          const unit = course.contents[j].course_contentable
          auxUserProgress.children[y].children.push({
            'name': unit.name,
            'icon': 'unit',
            'children': []
          });
          for (let k = 0; k < unit.contents.length; k++) {
            // add Lesson/Form
            const lessonForm = course.contents[j].course_contentable.contents[k];
            let auxIcon = lessonForm.unit_contentable_type.includes('Form') ? 'form' : 'lesson';
            let submits = auxIcon == 'form' ? lessonForm.unit_contentable.form_submits : [];
            let form_id = lessonForm.unit_contentable.id ?? undefined;

            auxUserProgress.children[y].children[j].children.push({
              'name': lessonForm.unit_contentable.name,
              'icon': auxIcon,
              'user_id': auxIcon == 'form' ? user.id : undefined,
              'form_id': auxIcon == 'form' ? form_id : undefined,
              'children': []
            });

            if (lessonForm.unit_contentable?.form_submits) {
              for (let l = 0; l < lessonForm.unit_contentable?.form_submits.length; l++) {
                const formSubmit = lessonForm.unit_contentable?.form_submits[l]
                if (user.id == formSubmit.user_id) {
                  auxUserProgress.children[y].children[j].children[k].children.push({
                    'id': formSubmit.id,
                    'name': formSubmit.date,
                    'icon': 'submit',
                    'link': `/groups/${state.group.id}/courses/${course.id}/units/${unit.id}/forms/${formSubmit.form_id}/submits/${formSubmit.id}/`,
                    'is_checked': formSubmit.is_checked,
                    'score': formSubmit.score
                  });
                }
              }
            }
          }
        }

      }
      // add auxUserProgress
      auxReturn.push(auxUserProgress);
    }
    // return formatedUserProgress
    return auxReturn;
  },
  getUserSubmitsByFormId: (state, getters) => (payload) => {
    let auxReturn = [];
    for (let i = 0; i < state.userFormSubmits.length; i++) {
      const user = state.userFormSubmits[i];
      if (user.id == payload.user_id) {
        for (let x = 0; x < user.form_submits.length; x++) {
          if (user.form_submits[x].form_id == payload.form_id) {
            auxReturn.push({
              'id': user.form_submits[x].id,
              'name': user.form_submits[x].date,
              'icon': 'submit',
              'link': `/groups/${payload.group_id}/courses/${payload.course_id}/units/${payload.unit_id}/forms/${payload.form_id}/submits/${user.form_submits[x].id}/`,
              'is_checked': user.form_submits[x].is_checked,
              'score': user.form_submits[x].score
            });
          }
        }
        break;
      }
    }
    // return getUserSubmitsByFormId
    return auxReturn;
  },
  getScoreBySubmitId: (state) => (payload) => {
    let auxReturn = '';
    for (let i = 0; i < state.userFormSubmits.length; i++) {
      const user = state.userFormSubmits[i];
      if (user.id == payload.user_id) {
        for (let x = 0; x < user.form_submits.length; x++) {
          const form_submit = user.form_submits[x];
          if (form_submit.id == payload.submit_id) {
            auxReturn = (
              (form_submit.correct_answers_count / form_submit.questions_count) * 100
            ).toFixed(1);
            break;
          }
        }
        break;
      }
    }
    // return getSubmitScore
    return auxReturn;
  }

}
