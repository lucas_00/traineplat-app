export default {
  async getGroups({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/group/index', data)
        commit('SET_GROUPS', response.data)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async createGroup({ commit, dispatch }, formData) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.post('/api/group/store', formData)
        commit('ADD_GROUP', response.data.group)
        await dispatch('layout/getLayoutConfig', null, { root: true })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },
  async deleteGroup({ dispatch }, group_id) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.delete('/api/group/destroy', {
          params: {
            group_id
          }
        })
        await dispatch('layout/getLayoutConfig', null, { root: true })
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
