export default {
  SET_GROUPS(state, data) {
    state.groups = data.groups;
  },
  ADD_GROUP(state, data) {
    state.groups.push(data);
  }
}
