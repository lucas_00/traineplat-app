import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    groups: []
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
