const getAnswer = (list, element) => {
  let index = list.findIndex(x => {
    return x.element?.id == element.id
  })
  return list[index]?.answers;
}

export default {
  SET_GROUP_FORM(state, data) {
    state.group = data.group;
    state.course = data.course;
    state.unit = data.unit;
    state.form = data.form;
  }
}
