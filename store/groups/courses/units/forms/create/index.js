import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    formContent: null
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
