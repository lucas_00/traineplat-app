export default {
  CLEAN_FORM_CONTENT(state, data) {
    state.formContent = {
      'form_contentable': {
        'fillintheblankable': [{

        }],
        'question': null
      },
      'form_contentable_type': "FillintheblankText",
      'order': 0
    };
  }
}
