export default {
  cleanFormContent({ commit }) {
    commit('CLEAN_FORM_CONTENT');
  }
}
