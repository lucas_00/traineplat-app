export default {
  async getGroupForm({ commit }, data) {
    return new Promise(async(resolve, reject)=>{
      try {
        const response = await this.$axios.get('/api/group/form', data)
        commit('SET_GROUP_FORM', response.data)

        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  },

  async submitAnswers({ state }, formContents) {
    return new Promise(async(resolve, reject)=>{
      try {
        let formData = new FormData();
        formData.append('form_contents', JSON.stringify(formContents));
        formData.append('group_id', state.group.id);
        formData.append('form_id', state.form.id);
        const response = await this.$axios.post('/api/form/submit_form', formData)
        resolve(response)

      } catch (e) {
        reject(e)
      }
    })
  }
}
