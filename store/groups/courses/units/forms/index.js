import actions from './actions'
import mutations from './mutations'

const state = function(){
  return {
    group: null,
    course: null,
    unit: null,
    form: null
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
