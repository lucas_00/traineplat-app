module.exports = {
  apps: [
    {
      name: 'TrainePlat App',
      exec_mode: 'cluster',
      instances: 'max', // Or a number of instances
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start'
    }
  ],
  deploy: {
    "production" : {
      user : "lucas",              // user used to authenticate
      host : "142.93.9.22",      // where to connect
      ref  : "origin/master",
      repo : "git@gitlab.com:lucas_00/traineplat-app.git",
      ssh_options: ['ForwardAgent=yes'],
      path : "/var/www/html/app.traineplat.com",
      'post-deploy' : 'nvm use && yarn && yarn build && pm2 startOrRestart ecosystem.config.js --env production'
    },
    "development" : {
      user : "lucas",              // user used to authenticate
      host : "localhost",      // where to connect
      ref  : "origin/master",
      repo : "git@gitlab.com:lucas_00/traineplat-app.git",
      ssh_options: ['ForwardAgent=yes', 'StrictHostKeyChecking=no'],
      path : "/Users/lucas-mb-air/development/traineplat/traineplat-app",
      'post-deploy' : 'nvm use && yarn && yarn build && pm2 startOrRestart ecosystem.config.js --env development'
    },
  }
}
