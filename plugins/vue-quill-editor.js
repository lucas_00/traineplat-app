import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'

require('quill/dist/quill.snow.css')
require('quill/dist/quill.core.css')

import { ImageDrop } from 'quill-image-drop-module';
import { ImageResize } from 'quill-image-resize-module';

Quill.register('modules/imageDrop', ImageDrop);

Vue.use(VueQuillEditor)
