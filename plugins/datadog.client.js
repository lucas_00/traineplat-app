import { datadogRum } from '@datadog/browser-rum';

export default ({$config: { ddApplicationId, ddApplicationToken, ddSite, ddService, ddVersion, ddEnv } }) => {
  datadogRum.init({
    applicationId: ddApplicationId,
    clientToken: ddApplicationToken,
    site: ddSite,
    service: ddService,
    version: ddVersion,
    env: ddEnv,
    sessionSampleRate: 100,
    sessionReplaySampleRate: 100,
    trackUserInteractions: true,
    trackResources: true,
    trackLongTasks: true,
    defaultPrivacyLevel:'mask-user-input'
  });
  datadogRum.startSessionReplayRecording();
}