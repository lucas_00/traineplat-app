import { mount } from '@vue/test-utils'
import AvatarMenu from '@/components/global/AvatarMenu.vue'

describe('AvatarMenu', () => {
  it('Convert user name to short hand string "Lucas Sena" --> "LS"', () => {
    const wrapper = mount(AvatarMenu, {
      mocks: {
        // Replace data value with this fake data
        '$auth': {
          user: {
            name: "Lucas Sena",
            email: "lucas.sena@outlook.com"
          }
        }
      }
    })
    expect(wrapper.find(`[data-testid="avatar-menu-short-name-button"]`).text()).toBe("LS");
  });
})
