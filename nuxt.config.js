import colors from 'vuetify/lib/util/colors'
var webpack = require('webpack')

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  server: {
    port: 3000, // default: 3000
    host: 'localhost' // default: localhost
  },
  telemetry: false,
  head: {
    titleTemplate: '%s - TrainePlat',
    title: 'TrainePlat',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  publicRuntimeConfig: {
    assetsURL: process.env.ASSETS_URL || 'http://localhost:8000',
    baseURL: process.env.BASE_URL || 'http://localhost:8000/api/',
    webURL: process.env.WEB_URL || 'http://localhost:3000',
    gtm: {
      id: process.env.GOOGLE_TAG_MANAGER_ID || 'GTM-XXXXXXX'
    },
    ddApplicationId: process.env.DD_APPLICATION_ID || '',
    ddApplicationToken: process.env.DD_CLIENT_TOKEN || '',
    ddSite: process.env.DD_SITE || '',
    ddService: process.env.DD_SERVICE || '',
    ddEnv: process.env.DD_ENV || '',
    ddVersion: process.env.DD_VERSION || '',
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '~/assets/main.css'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/vue-quill-editor', mode: 'client' },
    { src: '~/plugins/vue-easy-lightbox', mode: 'client' },
    '~/plugins/datadog.client.js'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    [
      '@nuxtjs/eslint-module', {
        fix: false,
        emitWarning: false,
        emitError: false
      }
    ],
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/toast',
    '@nuxtjs/gtm',
    'vue-social-sharing/nuxt',
    ['nuxt-clipboard', { autoSetContainer: true }]
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:8000',
    credentials: true
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: {
            url: '/api/logout',
            method: 'post',
            propertyName: false
          },
          user: {
            url: '/api/user',
            method: 'get',
            propertyName: 'false'
          }
        },
        token: {
          property: 'token',
          maxAge: false,
        },
      }
    },
    localStorage: false
  },

  gtm: {
    enabled: true,
    debug: false,
    layer: 'dataLayer',
    variables: {},

    pageTracking: true,
    pageViewEventName: 'nuxtRoute',

    autoInit: true,
    respectDoNotTrack: true,

    scriptId: 'gtm-script',
    scriptDefer: false,
    scriptURL: 'https://www.googletagmanager.com/gtm.js',
    crossOrigin: false,

    noscript: true,
    noscriptId: 'gtm-noscript',
    noscriptURL: 'https://www.googletagmanager.com/ns.html'
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    options: {
      customProperties: true
    },
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        light: {
          primary: colors.blue.lighten2,
          accent: colors.grey.lighten3,
          secondary: colors.amber.lighten3,
          info: colors.teal.lighten1,
          warning: colors.orange,
          error: colors.deepOrange.accent4,
          success: colors.green
        }
      }
    },
    defaultAssets: false,
    icons: {
      iconfont: 'mdiSvg',
    }
  },

  toast: {
    position: 'bottom-right',
    duration : 1500
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        'window.Quill': 'quill/dist/quill.js',
        'Quill': 'quill/dist/quill.js'
      })
    ],
    extend (config, ctx) {
      // config.resolve.alias['chart.js'] = 'chart.js/dist/Chart.js'
      config.externals = {
        moment: 'moment'
      }
    },
    extractCSS: {
      ignoreOrder: true
    },
    transpile: [
      /vuetify/,
      'defu'
    ],
  }
}
